# Folder Flow

Folder Flow is a methodoligy for naming files and folders within software projects. It was developed by a team of React developers, but you may find it works elsewhere.

Folder Flow aims to be both scalable and very simple to navigate. We achieve these goals quite simply by creating files in the base of the project, then using developer discretion to decide when to group related files.

It's a files-first approach, as opposed to folders-first. Imagine having 5 paper documents. You can flip through them or spread them on a desk for easy overview. But if you place between 1 and 2 files each into seperate envelopes, you'll need to open them all to get an overview. You maybe open 1-5 just to find one document in particular.

> The above analogy is exacerbated in software, because (a) our documents (files) import and refrence other documents, so we must look in other files just to understand one and (b) we put envelops (folders) within other envelopers. It's like we have to filing-cabinets full folders with documents that reference other documents in folders in other filing cabinets.

## Table of contents

<!-- MarkdownTOC -->

- The problems
  - Problem #1: Nested folders take longer to navigate
  - Problem #2: Having multiple index.js files open in tabs makes the tab titles identical
  - Problem #3: seperate `test` folders demote testing
  - Problem #4: imports are complicated when projects are too folder-happy
- The Folder Flow process
  - Stage 1: Your first files
  - Stage 2: More components and tooling
  - Stage 3: Even more components
  - Stage 4: Seperating commonly-used components
- Result
- Key concepts
- Annecdotes

<!-- /MarkdownTOC -->

---

## The problems

All React and coding projects seem to be a little different. Let's imagine one:

```
components/
  common/
    Logo/
      index.js
  Header/
    index.js
    Dropdown/
      index.js
      Item/
        index.js
        Icon/
          index.js
  App/
    index.js
  Footer/
    Footer.js
    Links/
      index.js
tests/
  common/
    Logo/
      index.test.js
  Header/
    index.js
    Dropdown/
      index.test.js
      Item/
        index.test.js
        Icon/
          index.test.js
  App/
    index.test.js
  Footer/
    index.test.js
    Links/
      index.test.js
config/
  webpack.config.js
  webpack.config.prod.js
  config.js
server.js
babel.config.js
package.json
```

> Note: in many react projects, there are also redux files seperated into `constance`, `actions` and `reducers` folders, as well as a parent `src` folder. I've actually simplified this quite bit.

### Problem #1: Nested folders take longer to navigate

Expanding any folder, such as `src`, then `components` and finally opening `index.js` simply takes longer. Moreover, ones many large directories are opened, the list of files and folder in the side bar becomes much longer, nested and harder to navigate.

### Problem #2: Having multiple index.js files open in tabs makes the tab titles identical

So you're clicking on `index.js` tabs or doing `ctrl+tab` to cycle through tabs all named `index.js`. Reading the file contents or finding the full path is required to guess which is the file you're looking for.

### Problem #3: seperate `test` folders demote testing

Tests should come first, not be hidden away in a `tests/` folder. They should be first-class citizens that live side-by-side with the code you want to test. Tests also server as documentation on how to use a component/file, so devs shouldn't have to navigate up multiple folder levels to a tests file to see usage examples. So:

```
components/
  Profile.js
  Profile.test.js
  ...
```

While test-coverage scripts help ensure no tests are left out, it's great to get a sense of coverage with a quick glance a folder.

### Problem #4: imports are complicated when projects are too folder-happy

An import in a typical app could look like `import SomeComponent from '../../../SomeCategory/SomeSection/SomeComponent'`. With a Folder Flow approach, it can be more like 'import SomeComponent from './SomeSection-SomeComponent'`.

While webpack, babel and perhaps even symlinks can help overcome this too, they require lengthy, error-prone configuration and upkeep of that configuration. Those solutions also may seem too "magical" and confusing to new developers who are used to relative paths for local code and strings for importing from `node_modules`.

Rather than re-invent the wheel, we can use Folder Flow.

## The Folder Flow process

### Stage 1: Your first files

```
App.js
App.test.js
babel.config.js
jest.config.js
package.json
server.js
webpack.config.js
```

At this stage, it's premature to hide any files in folders.

### Stage 2: More components and tooling

```
components/
  App.js
  App.test.js
  Header.js
  Header.test.js
  Header-Logo.js
  Header-Logo.test.js
config/
  config.js
  jest.config.js
  webpack.config.js
  webpack.config.prod.js
babel.config.js
package.json
server.js
```

Now we begin to have enough similar files for grouping. We only need to change a few import paths after doing so, easy with find/replace. THe hyphen in the hyphentated file names essentially replace a `/`, a folder.

### Stage 3: Even more components

```
components/
  Header/
    index.js
    Header.js
    Header.test.js
    Header-Logo.js
    Header-Logo.test.js
    Header-Dropdown.js
    Header-Dropdown.test.js
    Header-Dropdown-Item.js
    Header-Dropdown-Item.test.js
    Header-Dropdown-Item-Icon.js
    Header-Dropdown-Item-Icon.test.js
  App.js
  App.test.js
config/
  config.js
  jest.config.js
  webpack.config.js
  webpack.config.prod.js
babel.config.js
package.json
server.js
```

Now that we have so many `Header-` prefixed components, it makes sense to put them in a folder.

### Stage 4: Seperating commonly-used components

```
components/
  common/
    Logo.js
    Logo.test.js
  Header/
    index.js
    Header.js
    Header.test.js
    Header-Dropdown.js
    Header-Dropdown.test.js
    Header-Dropdown-Item.js
    Header-Dropdown-Item.test.js
    Header-Dropdown-Item-Icon.js
    Header-Dropdown-Item-Icon.test.js
  App.js
  App.test.js
  Footer.js
  Footer.test.js
  Footer-Links.js
  Footer-Links.test.js
config/
  webpack.config.js
  webpack.config.prod.js
  config.js
server.js
babel.config.js
package.json
```

In this stage, we've added a footer, which also needed an element nearly identical to what was in `Header/Header-Logo.js`. Now it's time to separate the `Logo` component to be more re-usable by the rest of the app. So we move it to a new folder called `common/`, then simply change a single import in `Header.js`.

## Result

16 fewer files/folders:

![diff](diff.png)

## Key concepts

* Manage complexity only once it arrises.

* Keep things visible--folders make things invisble--until that visibility becomes overwhelming.

* Move quickly. Allow single-use sub-components to be married to their parent. Only once you find another use for a nested component that can be used elsewhere do you need to make it a common component. With experience, you'll make these single-use componets re-usable already, so minimal change is required to un-nest the file.

* Limit setup. Javascript/React apps are notoriously configuration and tooling-heavy. Don't make that worse by making folder hierachies without justification.

## Annecdotes

* Every company I've joined, early or late-stage, has used or transitioned to this method. 